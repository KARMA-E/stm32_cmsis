TARGET_MCU    := cortex-m3
OUTPUT_NAME   := STM_project
FIRMWARE_NAME := stm_firmware
LSCRIPT_PATH  := ../cmsis/stm32f103c8tx.ld
VERSION       := 1_0

#----------------------------------------------------------------------------------------------------------------------

INCLUDE := \
../cmsis \
../src \
../src/self_tests

SOURCE := \
../cmsis \
../src

LIB_PATHS :=

LIBRARIES :=

GCC_DEF := 

#----------------------------------------------------------------------------------------------------------------------

OCD_INTERFACE := interface/stlink.cfg
OCD_TARGET    := target/stm32f1x.cfg

OCD_RESET_CMD := 
