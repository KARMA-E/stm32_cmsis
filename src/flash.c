#include "flash.h"
#include "usart.h"


void FLASH_Unlock(void)
{
	FLASH->KEYR = FLASH_KEY1;
	FLASH->KEYR = FLASH_KEY2;
	DBG_FILE_INFO();
}

void FLASH_Lock(void)
{
	FLASH->CR |= FLASH_CR_LOCK;
}

uint8_t FLASH_Check_ready(void)
{
	if((FLASH->SR & FLASH_SR_EOP) == FLASH_SR_EOP)
	{
		FLASH->SR &= ~FLASH_SR_EOP;
		return 1;
	}

	return 0;
}

void FLASH_Erase_page(uint32_t addr)
{
	FLASH->CR |= FLASH_CR_PER;
	FLASH->AR  = addr;
	FLASH->CR |= FLASH_CR_STRT;

	while(!FLASH_Check_ready()) {}

	FLASH->CR &= ~FLASH_CR_PER;
}

uint32_t FLASH_Read(uint32_t addr)
{
	return (*(volatile uint32_t*)addr);
}

void FLASH_Write(uint32_t addr, uint32_t data)
{
	FLASH->CR |= FLASH_CR_PG;
	*((volatile uint16_t*)addr) = (uint16_t)data;

	while(!FLASH_Check_ready()) {}

	addr += 2;
	data >>= 16;
	*((volatile uint16_t*)addr) = (uint16_t)data;

	while(!FLASH_Check_ready()) {}

	FLASH->CR &= ~FLASH_CR_PG;
}

void FLASH_Read_buf(uint32_t addr, uint32_t * buf, uint16_t len)
{
	while(len--)
	{
		*buf++ = FLASH_Read(addr);
		addr += 4;
	}
}

void FLASH_Write_buf(uint32_t addr, const uint32_t * buf, uint16_t len)
{
	while(len--)
	{
		FLASH_Write(addr, *buf++);
		addr += 4;
	}
}


