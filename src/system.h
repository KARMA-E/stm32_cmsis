#ifndef _SYSTEM_H_
#define _SYSTEM_H_


#include "ginc.h"

#define SYS_TIK_START()		TIM1->CR1 |= TIM_CR1_CEN
#define SYS_TIK_STOP()		TIM1->CR1 &= ~TIM_CR1_CEN;


void 		SYS_init(void);
uint32_t 	SYS_Get_tick_us(void);
uint32_t 	SYS_Get_tick_ms(void);
void 		SYS_Delay(uint32_t wait);

#endif
