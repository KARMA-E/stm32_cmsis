/*
 * dma.h
 *
 *  Created on: 30 мая 2022 г.
 *      Author: KARMA
 */

#ifndef _DMA_H_
#define _DMA_H_

#include "ginc.h"


void DMA_Init(void);
void DMA_M2P_Start(uint32_t mem_addr, uint32_t perif_addr, uint16_t data_qty);


#endif /* SRC_DMA_H_ */
