/*
 * flash_test.c
 *
 *  Created on: 27 окт. 2021 г.
 *      Author: KARMA
 */

#include "system.h"
#include "flash.h"
#include "usart.h"
#include "adc.h"
#include "flash_test.h"


#define _FLASH_TEST_DATA_SIZE	(FLASH_USER_DATA_SIZE / 2)
#define _FLASH_TEST_PAGES_QTY	(_FLASH_TEST_DATA_SIZE / FLASH_PAGE_SIZE)
#define _FLASH_SAVE_ADDR		(FLASH_USER_DATA_ADDR - FLASH_PAGE_SIZE)
#define _FLASH_SAVE_KEY			(0xAABBCCDD)

#define _GET_PAGE_ADDR(page)	(FLASH_USER_DATA_ADDR + ((uint32_t)page * FLASH_PAGE_SIZE))
#define _CALC_CHECK_VAL(a, b)	((uint32_t)a * (b / 4))

#define _CHECK_NO_ERR			(0)
#define _CHECK_ERASE_ERR		(1 << 0)
#define _CHECK_WRITE_ERR		(1 << 1)
#define _CHECK_READ_ERR			(1 << 2)


struct page_counters_s{
	uint32_t Erase_cnt;
	uint32_t Write_cnt;
	uint32_t Read_cnt;
};

struct page_statistic_s{
	struct page_counters_s Error;
	uint32_t Erase_all_cnt;
	uint32_t First_err_erase_qty;
	uint16_t Process_max_time;
	uint16_t Bad_words_max_qty;
	int8_t   Max_temperature;
};

struct check_status_s{
	uint8_t  enable;
	uint32_t cnt;
	uint32_t seed;
};


static struct page_statistic_s _page_statistic[_FLASH_TEST_PAGES_QTY];

static volatile struct check_status_s _check = {
	.enable = TEST_AUTO_START,
	.cnt = 0,
	.seed = 0x1234
};


_SI uint8_t _Get_bit_dif_qty(uint32_t val_1, uint32_t val_2)
{
	uint8_t res = 0;
	uint32_t xor = val_1 ^ val_2;

	while(xor != 0)
	{
		if((xor & 1) == 1)
		{
			res++;
		}
		xor >>= 1;
	}
	return res;
}


uint8_t FL_TEST_Check_page(uint16_t page, uint32_t seed)
{
	uint8_t status = 0;
	struct page_statistic_s * cur_stat = &_page_statistic[page];
	uint32_t start_time = SYS_Get_tick_ms();

	if(page < _FLASH_TEST_PAGES_QTY)
	{
		uint32_t page_addr = _GET_PAGE_ADDR(page);
		uint16_t cur_bad_words_qty = 0;
		FLASH_Erase_page(page_addr);

		//FLASH_Write(page_addr + 7 * 4, 0x11111111);

		for(uint16_t offset = 0; offset < FLASH_PAGE_SIZE; offset += 4)
		{
			volatile uint32_t data = FLASH_Read(page_addr + offset);


			if(data != FLASH_INVALID_DWORD)
			{
#if !TEST_AUTO_START
				SYS_TIK_STOP();
				DBG("\r\n  Page #%d erase %d dword %03d - erase check error", page, cur_stat->Erase_all_cnt, offset / 4);
				DBG(" -> %08X != %08X (%d bits)", data, FLASH_INVALID_DWORD, _Get_bit_dif_qty(data, FLASH_INVALID_DWORD));
				SYS_TIK_START();
#endif
				status |= _CHECK_ERASE_ERR;
			}

			uint32_t calc_val = _CALC_CHECK_VAL(seed, offset);
			FLASH_Write(page_addr + offset, calc_val);


			if((FLASH->SR & FLASH_SR_PGERR) == FLASH_SR_PGERR)
			{
#if !TEST_AUTO_START
				SYS_TIK_STOP();
				FLASH->SR &= ~FLASH_SR_PGERR;
				DBG("\r\n  Page #%d erase %d dword %03d - write check error", page, cur_stat->Erase_all_cnt, offset / 4);
				SYS_TIK_START();
#endif
				status |= _CHECK_WRITE_ERR;
			}

		}

		for(uint16_t offset = 0; offset < FLASH_PAGE_SIZE; offset += 4)
		{
			uint32_t data = FLASH_Read(page_addr + offset);
			uint32_t calc_val = _CALC_CHECK_VAL(seed, offset);


			if(data != calc_val)
			{
#if !TEST_AUTO_START
				SYS_TIK_STOP();
				DBG("\r\n  Page #%d erase %d dword %03d - read  check error", page, cur_stat->Erase_all_cnt, offset / 4);
				DBG(" -> %08X != %08X (%d bits)", data, calc_val, _Get_bit_dif_qty(data, calc_val));
				SYS_TIK_START();
#endif
				cur_bad_words_qty++;
				status |= _CHECK_READ_ERR;
			}
		}

		uint16_t cur_process_time = (uint16_t)(SYS_Get_tick_ms() - start_time);
		cur_stat->Erase_all_cnt++;

		if((status & _CHECK_ERASE_ERR) == _CHECK_ERASE_ERR) {cur_stat->Error.Erase_cnt++;}
		if((status & _CHECK_WRITE_ERR) == _CHECK_WRITE_ERR) {cur_stat->Error.Write_cnt++;}
		if((status & _CHECK_READ_ERR ) == _CHECK_READ_ERR ) {cur_stat->Error.Read_cnt++;}


		if((cur_stat->First_err_erase_qty == 0) && (status != _CHECK_NO_ERR))
		{
			SYS_TIK_STOP();
			cur_stat->First_err_erase_qty = cur_stat->Erase_all_cnt;
			DBG("\r\n  Page %d detect first error %d", page, cur_stat->First_err_erase_qty);
			SYS_TIK_START();
		}


		if(cur_process_time > cur_stat->Process_max_time)
		{
			cur_stat->Process_max_time = cur_process_time;
		}

		if(cur_bad_words_qty > cur_stat->Bad_words_max_qty)
		{
			cur_stat->Bad_words_max_qty = cur_bad_words_qty;
		}

		int16_t temperature_int;
		uint8_t temperature_fract;
		ADC_Get_temp(ADC1, &temperature_int, &temperature_fract);

		if(temperature_int > cur_stat->Max_temperature)
		{
			cur_stat->Max_temperature = temperature_int;
		}
	}
	else
	{
		DBG("\r\n  Page %d out of userpage range 0 ... %d", page, _FLASH_TEST_PAGES_QTY);
	}

	return status;
}

void FL_TEST_Pull(void)
{

	if(_check.enable == 1)
	{
		for(uint8_t page = 0; page < 10; page++)
		{
			FL_TEST_Check_page(page, _check.seed);
			//uint32_t wait = 10000;
			//while(--wait);
			_check.seed = (_check.seed + 7) * 13;
		}

		_check.cnt++;

		if((_check.cnt % 50) == 0)
		{
			uint32_t ticks = SYS_Get_tick_ms();
			struct data_time_s{
				uint8_t sec;
				uint8_t min;
				uint8_t hour;
				uint8_t day;
			}time;

			time.day  = ticks / (24 * 60 * 60 * 1000UL);	ticks %= (24 * 60 * 60 * 1000UL);
			time.hour = ticks / (60 * 60 * 1000UL);		 	ticks %= (60 * 60 * 1000UL);
			time.min  = ticks / (60 * 1000UL);			 	ticks %= (60 * 1000UL);
			time.sec  = ticks / (1000UL);

			DBG("\r\nCheck #%03d | seed: %08X | Time: %02ud-%02uh-%02um-%02us",
					_check.cnt, _check.seed, time.day, time.hour, time.min, time.sec);
		}

		if((_check.cnt % 5000) == 0)
		{
			FL_TEST_Save_statistic();
		}
	}
}

void FL_TEST_Set_enable(uint8_t enable)
{
	_check.enable = enable;
}

void FL_TEST_Save_statistic(void)
{
	uint32_t time = SYS_Get_tick_us();

	FLASH_Erase_page(_FLASH_SAVE_ADDR);
	FLASH_Write_buf(_FLASH_SAVE_ADDR, (uint32_t*)_page_statistic, sizeof(_page_statistic) / 4);
	FLASH_Write(_FLASH_SAVE_ADDR + sizeof(_page_statistic), _FLASH_SAVE_KEY);

	time = SYS_Get_tick_us() - time;
	DBG("\r\nSave statistic ... OK");
	DBG(" (%d us)", time);
}

void FL_TEST_Read_statistic(void)
{
	DBG("\r\nStatistic size: %d bytes", sizeof(_page_statistic));
	DBG("\r\nRead statistic ... ");
	uint32_t time = SYS_Get_tick_us();
	uint32_t check_key = FLASH_Read(_FLASH_SAVE_ADDR + sizeof(_page_statistic));

	if(check_key == _FLASH_SAVE_KEY)
	{
		FLASH_Read_buf(_FLASH_SAVE_ADDR, (uint32_t*)_page_statistic, sizeof(_page_statistic) / 4);
		time = SYS_Get_tick_us() - time;
		DBG("OK");
	}
	else
	{
		time = SYS_Get_tick_us() - time;
		DBG("ERROR");
	}
	DBG(" (%d us)", time);
}

void FL_TEST_Delete_statistic(void)
{
	for(uint32_t i = 0; i < sizeof(_page_statistic) / 4; i++)
	{
		((uint32_t*)_page_statistic)[i] = 0;
	}
	DBG("\r\nReset statistic (RAM)");

	FL_TEST_Save_statistic();
}

void FL_TEST_Print_statistic(void)
{
	DBG("\r\n");

	for(uint16_t page = 0; page < _FLASH_TEST_PAGES_QTY; page++)
	{
		struct page_statistic_s * cur_stat = &_page_statistic[page];

		DBG("\r\nPage #%02d erase: %u || MaxTime: %02u ms | MaxBad: %02u | Temp: %d | FirstErr: %u | Errors - %u/%u/%u (e/w/r)",
		     page, cur_stat->Erase_all_cnt, cur_stat->Process_max_time, cur_stat->Bad_words_max_qty, cur_stat->Max_temperature,
			 cur_stat->First_err_erase_qty, cur_stat->Error.Erase_cnt, cur_stat->Error.Write_cnt, cur_stat->Error.Read_cnt);
	}
}

