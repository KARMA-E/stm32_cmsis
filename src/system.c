#include "system.h"
#include "usart.h"

static volatile uint32_t _tick_reload_cnt = 0;

#define _RAM_BUF_SIZE		(0x1000)
#define _RAM_BUF_START		(0x20005000 - RAM_BUF_SIZE)

#define _TIM_ARR_VAL		(65000)


_SI void _GPIO_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;

	MODIFY_REG(GPIOA->CRL, GPIO_CRL_MODE0_Msk, GPIO_MODE_50_MHZ << GPIO_CRL_MODE0_Pos);
	MODIFY_REG(GPIOA->CRL, GPIO_CRL_CNF0_Msk,  GPIO_CNF_GEN_PP  << GPIO_CRL_CNF0_Pos);

	MODIFY_REG(GPIOC->CRH, GPIO_CRH_MODE13_Msk, GPIO_MODE_2_MHZ << GPIO_CRH_MODE13_Pos);
	MODIFY_REG(GPIOC->CRH, GPIO_CRH_CNF13_Msk,  GPIO_CNF_GEN_PP << GPIO_CRH_CNF13_Pos);

	MODIFY_REG(GPIOC->CRH, GPIO_CRH_MODE14_Msk, GPIO_MODE_2_MHZ << GPIO_CRH_MODE14_Pos);
	MODIFY_REG(GPIOC->CRH, GPIO_CRH_CNF14_Msk,  GPIO_CNF_GEN_PP << GPIO_CRH_CNF14_Pos);
}

_SI void _MCO_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	MODIFY_REG(GPIOA->CRH, GPIO_CRH_MODE8_Msk, GPIO_MODE_10_MHZ << GPIO_CRH_MODE8_Pos);
	MODIFY_REG(GPIOA->CRH, GPIO_CRH_CNF8_Msk,  GPIO_CNF_ALT_PP << GPIO_CRH_CNF8_Pos);

	MODIFY_REG(RCC->CFGR, RCC_CFGR_MCO_Msk, RCC_CFGR_MCOSEL_SYSCLK);
}

_SI void _CLK_init(void)
{
	RCC->CR |= RCC_CR_HSEON;
	while((RCC->CR & RCC_CR_HSERDY) != RCC_CR_HSERDY){}

	RCC->CFGR |= RCC_CFGR_PLLSRC;
	MODIFY_REG(RCC->CFGR, RCC_CFGR_PLLXTPRE_Msk, RCC_CFGR_PLLXTPRE_HSE);
	MODIFY_REG(RCC->CFGR, RCC_CFGR_PLLMULL_Msk, (PLL_MULT_VAL - 2) << RCC_CFGR_PLLMULL_Pos);

	RCC->CR |= RCC_CR_PLLON;
	while((RCC->CR & RCC_CR_PLLRDY) != RCC_CR_PLLRDY){}

	MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE1_Msk,  PPRE1_DIV);
	MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE2_Msk,  PPRE2_DIV);
	MODIFY_REG(RCC->CFGR, RCC_CFGR_ADCPRE_Msk, ADCPRE_DIV);

	MODIFY_REG(FLASH->ACR, FLASH_ACR_LATENCY_Msk, FLASH_ACR_LATENCY_1);
	MODIFY_REG(RCC->CFGR, RCC_CFGR_SW_Msk, RCC_CFGR_SW_PLL);
}

_SI void _Tick_init(void)
{
	SysTick->CTRL = 0;
	//SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;
	SysTick->LOAD = ((FREQ_SYS_MHZ * 1000) / 8) - 1;
	NVIC_EnableIRQ(SysTick_IRQn);
	//NVIC_SetPriority(FLASH_IRQn, 2);

	SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk;
}

_SI void _TIM1_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
	TIM1->DIER |= TIM_DIER_UIE;
	TIM1->PSC = 47;
	TIM1->ARR = _TIM_ARR_VAL - 1;

	NVIC_EnableIRQ(TIM1_UP_IRQn );

	TIM1->CR1 |= TIM_CR1_CEN;
}


void SYS_init(void)
{
	_GPIO_init();
	_CLK_init();
	_MCO_init();
	_Tick_init();
	_TIM1_init();

	__enable_irq();

}

uint32_t SYS_Get_tick_us(void)
{
	uint32_t check_cnt = _tick_reload_cnt;
	volatile uint32_t res = (_tick_reload_cnt * _TIM_ARR_VAL) + TIM1->CNT;

	if(check_cnt != _tick_reload_cnt)
	{
		res = (_tick_reload_cnt * _TIM_ARR_VAL) + TIM1->CNT;
	}

	return res;
}

uint32_t SYS_Get_tick_ms(void)
{
	uint32_t check_cnt = _tick_reload_cnt;
	volatile uint32_t res = (_tick_reload_cnt * 65) + (TIM1->CNT / 1000);

	if(check_cnt != _tick_reload_cnt)
	{
		res = (_tick_reload_cnt * 65) + (TIM1->CNT / 1000);
	}

	return res;
}

void SYS_Delay(uint32_t wait)
{
	uint32_t time = SYS_Get_tick_ms();
	uint32_t new_time = SYS_Get_tick_ms();

	while((new_time < (time + wait)) && (time <= new_time))
	{
		new_time = SYS_Get_tick_ms();
	}
}


void SysTick_Handler(void)
{
	//_tick_reload_cnt++;
}

void UsageFault_Handler(void)
{
	DBG("\r\n\r\nEnter in %s()", __func__);
}

void TIM1_UP_IRQHandler(void)
{
	_tick_reload_cnt++;
	TIM1->SR &= ~TIM_SR_UIF;
	TIM1->SR &= ~TIM_SR_UIF;	//TODO: Разобраться, почему без этой строки прерывание срабатывает два раза
}



