#ifndef _ADC_H_
#define _ADC_H_

#include "ginc.h"


uint8_t ADC_Init(ADC_TypeDef * ADCx);
uint8_t ADC_Set_channel_sample(ADC_TypeDef * ADCx, uint8_t channel, uint8_t smp);

uint8_t ADC_Set_channel_rsq(ADC_TypeDef * ADCx, uint8_t channel, uint8_t sq);
uint8_t ADC_Set_channel_jsq(ADC_TypeDef * ADCx, uint8_t channel, uint8_t sq);

uint8_t ADC_Set_rsq_qty(ADC_TypeDef * ADCx, uint8_t qty);
uint8_t ADC_Set_jsq_qty(ADC_TypeDef * ADCx, uint8_t qty);

void ADC_Start_regular(ADC_TypeDef * ADCx);
void ADC_Start_injected(ADC_TypeDef * ADCx);

uint8_t ADC_Get_regular(ADC_TypeDef * ADCx, uint16_t * val);
uint8_t ADC_Get_injected(ADC_TypeDef * ADCx, uint16_t * val);
uint8_t ADC_Get_temp(ADC_TypeDef * ADCx, int16_t * temp_int, uint8_t * temp_fract);

#endif
