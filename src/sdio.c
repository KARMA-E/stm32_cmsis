/*
 * sdio.c
 *
 *  Created on: 24 мая 2022 г.
 *      Author: KARMA
 */
#include "ginc.h"
#include "system.h"
#include "usart.h"


#define _GPIO_CS_PORT		(GPIOA)
#define _GPIO_CS_PIN		(0)
#define _GPIO_SCK_PORT		(GPIOA)
#define _GPIO_SCK_PIN		(2)
#define _GPIO_MOSI_PORT		(GPIOA)
#define _GPIO_MOSI_PIN		(1)
#define _GPIO_MISO_PORT		(GPIOA)
#define _GPIO_MISO_PIN		(3)



static void _Gpio_init(void)
{
	uint32_t pin_offset = _GPIO_CS_PIN * GPIO_CR_BITS_PER_PIN;

	MODIFY_REG(_GPIO_CS_PORT->CRL, GPIO_MODE_Msk << pin_offset, GPIO_MODE_2_MHZ << pin_offset);
	MODIFY_REG(_GPIO_CS_PORT->CRL, GPIO_CNF_Msk << (pin_offset + 2), GPIO_CNF_GEN_PP << (pin_offset + 2));

}


void SDIO_Init(void)
{
	_Gpio_init();



	DBG_FILE_INFO();
}

void SDIO_TestCs(void)
{
	_GPIO_CS_PORT->ODR ^= 1 << _GPIO_CS_PIN;
}
